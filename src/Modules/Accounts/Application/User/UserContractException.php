<?php
declare(strict_types=1);

namespace App\Modules\Accounts\Application\User;

use Exception;

final class UserContractException extends Exception
{
}
