<?php
declare(strict_types=1);

namespace App\Web\API\Action;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

abstract class AbstractAction
{
    protected function noContentResponse(): JsonResponse
    {
        return new JsonResponse([], Response::HTTP_NO_CONTENT);
    }

    protected function json(array $data): JsonResponse
    {
        return new JsonResponse($data, Response::HTTP_OK);
    }
}
